int ledOne = D2;        // PWM PIN for LED 1
int ledTwo = D5;        // PWM PIN for LED 2
int ledThree = D3;        // PWM PIN for LED 3
int ledFour = D8;        // PWM PIN for LED 4
int dial = A0;          // Input pin for potentiometer

// the setup routine runs once when you press reset:
void setup() {
  pinMode(dial, INPUT); // Set potentiometer pin to input mode
  pinMode(ledOne, OUTPUT); // Set LED one pin to output mode
  pinMode(ledTwo, OUTPUT); // Set LED two pin to output mode
  pinMode(ledThree, OUTPUT); // Set LED three pin to output mode
  pinMode(ledFour, OUTPUT); // Set LED fourth pin to output mode
}

// the loop routine runs over and over again forever:
void loop() {
  int ledValueOne = map(analogRead(dial), 0, 256, 0, 1023); // Constrain value of potentiometer to write to LED one
  int ledValueTwo = map(analogRead(dial), 256, 512, 0, 1023); // Constrain value of potentiometer to write to LED two
  int ledValueThree = map(analogRead(dial), 512, 768, 0, 1023); // Constrain value of potentiometer to write to LED three
  int ledValueFour = map(analogRead(dial), 768, 1024, 0, 1023); // Constrain value of potentiometer to write to LED four

  analogWrite(ledOne, ledValueOne); //Write to first LED
  analogWrite(ledTwo, ledValueTwo); //Write to second LED
  analogWrite(ledThree, ledValueThree); //Write to third LED
  analogWrite(ledFour, ledValueFour); //Write to fourth LED

  delay(10); //Slight delay
}

