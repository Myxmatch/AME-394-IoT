#include <Wire.h>
#include <LiquidCrystal_I2C.h>
#include <ESP8266WiFi.h>

const char* ssid     = "Tejaswi";
const char* password = "12345678";

const char* host = "54.167.51.230";
LiquidCrystal_I2C lcd(0x3F, 16, 2);
void setup() {
  Serial.begin(115200);
  delay(10);
    pinMode(D2, OUTPUT);


  // We start by connecting to a WiFi network

  Serial.println();
  Serial.println();
  Serial.print("Connecting to ");
  Serial.println(ssid);

  /* Explicitly set the ESP8266 to be a WiFi-client, otherwise, it by default,
     would try to act as both a client and an access-point and could cause
     network-issues with your other WiFi-devices on your WiFi-network. */
  WiFi.mode(WIFI_STA);
  WiFi.begin(ssid, password);

  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
  }
      lcd.begin();
      lcd.backlight();
  print2Screen("Test line 1", "Test line 2");
}

int value = 0;
void print2Screen(String s1, String s2){
  lcd.print(s1);
  lcd.setCursor(0,1);
  lcd.print(s2);
}

void loop() {
  delay(500);
  ++value;

  Serial.print("connecting to ");
  Serial.println(host);

  // Use WiFiClient class to create TCP connections
  WiFiClient client;
  const int httpPort = 80;
  if (!client.connect(host, httpPort)) {
    Serial.println("connection failed");
    return;
  }

  // We now create a URI for the request
  String path = "/getValue";


  Serial.print("Requesting URL: ");
  Serial.println(path);

  // This will send the request to the server
  client.print(String("GET ") + path + " HTTP/1.1\r\n" +
               "Host: " + host + "\r\n" +
               "Connection: close\r\n\r\n");
  unsigned long timeout = millis();
  while (client.available() == 0) {
    if (millis() - timeout > 5000) {
      Serial.println(">>> Client Timeout !");
      client.stop();
      return;
    }
  }
  int lineno = 1;
  // Read all the lines of the reply from server and print them to Serial
  while (client.available()) {
    String line = client.readStringUntil('\r');
    String val = line;
    
    if(lineno == 9){
      lcd.clear();
     int firstLimit = val.indexOf(',');
     String firstLine = val.substring(1, firstLimit);
     Serial.print(firstLine);
      String secondLine = val.substring(firstLimit+1);
      Serial.print(secondLine);
      print2Screen(firstLine, secondLine);
    }
    else {
      
    }
    lineno = lineno + 1;
  }

  Serial.println();
  Serial.println("closing connection");
}

