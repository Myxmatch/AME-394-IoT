#include <Arduino.h>

extern "C" {
#include "user_interface.h"
}

//set wifi access point credentials
char * HOSTNAME = "Garrett Miller";
char * WifiPASS = "Test1234";

#include <ESP8266WiFi.h>
#include <ESP8266WebServer.h>
#include <Hash.h>

//set up server to be on port 80
ESP8266WebServer server (80);

//initiate led pen variable
int ledPin = D2; 

void setup() {
    Serial.begin(115200);

    pinMode(ledPin, OUTPUT);

    // Start Wifi AP
    WiFi.mode(WIFI_AP_STA);
    WiFi.softAP(HOSTNAME, WifiPASS);

    //url for 0% brightness
    server.on("/zero", []() {
      analogWrite(ledPin, 0);
      server.send(200, "text/html", "<html><head></head><body><a href=\"./zero\">0%</a><br><a href=\"./ten\">10%</a><br><a href=\"./twenty\">20%</a><br><a href=\"./thirty\">30%</a><br><a href=\"./fourty\">40%</a><br><a href=\"./fifty\">50%</a><br><a href=\"./sixty\">60%</a><br><a href=\"./seventy\">70%</a><br><a href=\"./eighty\">80%</a><br><a href=\"./ninety\">90%</a><br><a href=\"./hundred\">100%</a></body></html>");
    });

    //url for 10% brightness
    server.on("/ten", []() {
      analogWrite(ledPin, 102);
      server.send(200, "text/html", "<html><head></head><body><a href=\"./zero\">0%</a><br><a href=\"./ten\">10%</a><br><a href=\"./twenty\">20%</a><br><a href=\"./thirty\">30%</a><br><a href=\"./fourty\">40%</a><br><a href=\"./fifty\">50%</a><br><a href=\"./sixty\">60%</a><br><a href=\"./seventy\">70%</a><br><a href=\"./eighty\">80%</a><br><a href=\"./ninety\">90%</a><br><a href=\"./hundred\">100%</a></body></html>");
    });

    //url for 20% brightness
    server.on("/twenty", []() {
      analogWrite(ledPin, 205);
      server.send(200, "text/html", "<html><head></head><body><a href=\"./zero\">0%</a><br><a href=\"./ten\">10%</a><br><a href=\"./twenty\">20%</a><br><a href=\"./thirty\">30%</a><br><a href=\"./fourty\">40%</a><br><a href=\"./fifty\">50%</a><br><a href=\"./sixty\">60%</a><br><a href=\"./seventy\">70%</a><br><a href=\"./eighty\">80%</a><br><a href=\"./ninety\">90%</a><br><a href=\"./hundred\">100%</a></body></html>");
    });

    //url for 30% brightness
    server.on("/thirty", []() {
      analogWrite(ledPin, 307);
      server.send(200, "text/html", "<html><head></head><body><a href=\"./zero\">0%</a><br><a href=\"./ten\">10%</a><br><a href=\"./twenty\">20%</a><br><a href=\"./thirty\">30%</a><br><a href=\"./fourty\">40%</a><br><a href=\"./fifty\">50%</a><br><a href=\"./sixty\">60%</a><br><a href=\"./seventy\">70%</a><br><a href=\"./eighty\">80%</a><br><a href=\"./ninety\">90%</a><br><a href=\"./hundred\">100%</a></body></html>");
    });

    //url for 40% brightness
    server.on("/fourty", []() {
      analogWrite(ledPin, 408);
      server.send(200, "text/html", "<html><head></head><body><a href=\"./zero\">0%</a><br><a href=\"./ten\">10%</a><br><a href=\"./twenty\">20%</a><br><a href=\"./thirty\">30%</a><br><a href=\"./fourty\">40%</a><br><a href=\"./fifty\">50%</a><br><a href=\"./sixty\">60%</a><br><a href=\"./seventy\">70%</a><br><a href=\"./eighty\">80%</a><br><a href=\"./ninety\">90%</a><br><a href=\"./hundred\">100%</a></body></html>");
    });

    //url for 50% brightness
    server.on("/fifty", []() {
      analogWrite(ledPin, 512);
      server.send(200, "text/html", "<html><head></head><body><a href=\"./zero\">0%</a><br><a href=\"./ten\">10%</a><br><a href=\"./twenty\">20%</a><br><a href=\"./thirty\">30%</a><br><a href=\"./fourty\">40%</a><br><a href=\"./fifty\">50%</a><br><a href=\"./sixty\">60%</a><br><a href=\"./seventy\">70%</a><br><a href=\"./eighty\">80%</a><br><a href=\"./ninety\">90%</a><br><a href=\"./hundred\">100%</a></body></html>");
    });

    //url for 60% brightness
    server.on("/sixty", []() {
      analogWrite(ledPin, 614);
      server.send(200, "text/html", "<html><head></head><body><a href=\"./zero\">0%</a><br><a href=\"./ten\">10%</a><br><a href=\"./twenty\">20%</a><br><a href=\"./thirty\">30%</a><br><a href=\"./fourty\">40%</a><br><a href=\"./fifty\">50%</a><br><a href=\"./sixty\">60%</a><br><a href=\"./seventy\">70%</a><br><a href=\"./eighty\">80%</a><br><a href=\"./ninety\">90%</a><br><a href=\"./hundred\">100%</a></body></html>");
    });

    //url for 70% brightness
    server.on("/seventy", []() {
      analogWrite(ledPin, 717);
      server.send(200, "text/html", "<html><head></head><body><a href=\"./zero\">0%</a><br><a href=\"./ten\">10%</a><br><a href=\"./twenty\">20%</a><br><a href=\"./thirty\">30%</a><br><a href=\"./fourty\">40%</a><br><a href=\"./fifty\">50%</a><br><a href=\"./sixty\">60%</a><br><a href=\"./seventy\">70%</a><br><a href=\"./eighty\">80%</a><br><a href=\"./ninety\">90%</a><br><a href=\"./hundred\">100%</a></body></html>");
    });

    //url for 80% brightness
    server.on("/eighty", []() {
      analogWrite(ledPin, 819);
      server.send(200, "text/html", "<html><head></head><body><a href=\"./zero\">0%</a><br><a href=\"./ten\">10%</a><br><a href=\"./twenty\">20%</a><br><a href=\"./thirty\">30%</a><br><a href=\"./fourty\">40%</a><br><a href=\"./fifty\">50%</a><br><a href=\"./sixty\">60%</a><br><a href=\"./seventy\">70%</a><br><a href=\"./eighty\">80%</a><br><a href=\"./ninety\">90%</a><br><a href=\"./hundred\">100%</a></body></html>");
    });

    //url for 90% brightness
    server.on("/ninety", []() {
      analogWrite(ledPin, 922);
      server.send(200, "text/html", "<html><head></head><body><a href=\"./zero\">0%</a><br><a href=\"./ten\">10%</a><br><a href=\"./twenty\">20%</a><br><a href=\"./thirty\">30%</a><br><a href=\"./fourty\">40%</a><br><a href=\"./fifty\">50%</a><br><a href=\"./sixty\">60%</a><br><a href=\"./seventy\">70%</a><br><a href=\"./eighty\">80%</a><br><a href=\"./ninety\">90%</a><br><a href=\"./hundred\">100%</a></body></html>");
    });
    
    //url for 100% brightness
    server.on("/hundred", []() {
      analogWrite(ledPin, 1024);
      server.send(200, "text/html", "<html><head></head><body><a href=\"./zero\">0%</a><br><a href=\"./ten\">10%</a><br><a href=\"./twenty\">20%</a><br><a href=\"./thirty\">30%</a><br><a href=\"./fourty\">40%</a><br><a href=\"./fifty\">50%</a><br><a href=\"./sixty\">60%</a><br><a href=\"./seventy\">70%</a><br><a href=\"./eighty\">80%</a><br><a href=\"./ninety\">90%</a><br><a href=\"./hundred\">100%</a></body></html>");
    });

    //url for default homepage
    server.on("/", []() {
      server.send(200, "text/html", "<html><head></head><body><a href=\"./zero\">0%</a><br><a href=\"./ten\">10%</a><br><a href=\"./twenty\">20%</a><br><a href=\"./thirty\">30%</a><br><a href=\"./fourty\">40%</a><br><a href=\"./fifty\">50%</a><br><a href=\"./sixty\">60%</a><br><a href=\"./seventy\">70%</a><br><a href=\"./eighty\">80%</a><br><a href=\"./ninety\">90%</a><br><a href=\"./hundred\">100%</a></body></html>");
    });

    //start server
    server.begin();
    
}

void loop() {
    server.handleClient();
}

