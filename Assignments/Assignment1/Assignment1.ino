String inData;          //Data received for brightness input
int ledOne = D2;        // PWM PIN for LED 1

void setup() {
  // initialize serial:
  Serial.begin(115200);
  pinMode(ledOne, OUTPUT);
}

void loop() {
  
    while (Serial.available() > 0)
    {
        char recieved = Serial.read();
        inData += recieved; 

        // Process message when new line character is recieved
        if (recieved == '\n')
        {
          int x = inData.toInt();

          Serial.print("Message Received: "  + inData); // Print input value
          
          int ledValueOne = map(x, 0, 100, 0, 1023); // Constrain value of input to write to LED one

          Serial.print("Setting brightness to: " + String(ledValueOne) + "\n"); //Print brightness value
          
          analogWrite(ledOne, ledValueOne); // Write to first LED
          
          inData = ""; // Clear recieved buffer
        }
    }
}
