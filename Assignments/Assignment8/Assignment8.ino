#include <Servo.h>

Servo needle;

int needleValT = 0;
int needleValH = 0;

int tLight = D7;
int hLight = D8;

#include <SimpleDHT.h>
int pinDHT11 = D4;
SimpleDHT11 dht11(pinDHT11);

float t = 0;
float h = 0;

int buttonTState = 0;
int buttonHState = 0;

void setup() {
  Serial.begin(115000);

  needle.attach(D2);

  pinMode(tLight, OUTPUT);
  pinMode(hLight, OUTPUT);

  pinMode(D5, INPUT_PULLUP);
  pinMode(D6, INPUT_PULLUP);


}

void loop() {
  // put your main code here, to run repeatedly:
  byte temperature = 0;
  byte humidity = 0;
  int err = SimpleDHTErrSuccess;
  if ((err = dht11.read(&temperature, &humidity, NULL)) != SimpleDHTErrSuccess) {
    Serial.print("Read DHT11 failed, err="); Serial.println(err); delay(1000);
    return;
  }

  Serial.print("Sample OK: ");
  Serial.print((int)temperature); Serial.print(" *C, ");
  Serial.print((int)humidity); Serial.println(" H");

  t = (float)temperature * 1.8 + 32;
  h = (float)humidity;

  needleValT = map(t, 0, 160, 30, 150);
  needleValH = map(t, 0, 100, 0, 100);

  int buttonT = digitalRead(D5);
  Serial.println(buttonT);
  Serial.println(needleValT);
  Serial.println(needleValH);
  int buttonH = digitalRead(D6);
  Serial.println(buttonH);

  if (buttonT == 1) {
    buttonHState = 0;
    buttonTState = 1;
  }

  if (buttonH == 1) {
    buttonHState = 1;
    buttonTState = 0;
  }
  if(buttonTState == 1) {
    needle.write(needleValT);
    digitalWrite(tLight, HIGH);
    digitalWrite(hLight, LOW);
  }
  else {
    needle.write(needleValH);
    digitalWrite(tLight, LOW);
    digitalWrite(hLight, HIGH);
  }

}
