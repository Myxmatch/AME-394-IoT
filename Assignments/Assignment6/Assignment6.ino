#include <Wire.h> 
#include <LiquidCrystal_I2C.h>
#include <SimpleDHT.h>
#include <ESP8266WiFi.h>
LiquidCrystal_I2C lcd(0x3F, 16, 2);

const char* ssid     = "Tejaswi";
const char* password = "12345678";

const char* host = "34.219.221.194";

int pinDHT11 = D4;
SimpleDHT11 dht11(pinDHT11);

void setup()
{
	// initialize the LCD
  Serial.begin(115200);
  pinMode(D2, OUTPUT);
	lcd.begin();
  lcd.backlight();
  WiFi.mode(WIFI_STA);
  WiFi.begin(ssid, password);

  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
  }
}

void print2Screen(String s1, String s2){
  lcd.home();
  lcd.clear();
  lcd.print(s1);
  lcd.setCursor(0,1);
  lcd.print(s2);
}
void loop()
{
  byte temperature = 0;
  byte humidity = 0;
  int err = SimpleDHTErrSuccess;
  if ((err = dht11.read(&temperature, &humidity, NULL)) != SimpleDHTErrSuccess) {
    Serial.print("Read DHT11 failed, err="); Serial.println(err);delay(1000);
    return;
  }
  
  Serial.print("Sample OK: ");
  Serial.print((int)temperature); Serial.print(" *C, "); 
  Serial.print((int)humidity); Serial.println(" H");

  print2Screen("Temp: " + String(round(temperature * 1.8 + 32)) + (char)223 +"F", "Humidity: " + String(humidity) + "%");

  WiFiClient client;
  const int httpPort = 80;
  if (!client.connect(host, httpPort)) {
    Serial.println("connection failed");
    return;
  }

  // We now create a URI for the request
  String path = "/setValue";


  Serial.print("Requesting URL: ");
  Serial.println(path);

  // This will send the request to the server
  client.print(String("GET ") + path + "?t=" + String(round(temperature * 1.8 + 32)) + "&h=" + String(humidity) + " HTTP/1.1\r\n" +
               "Host: " + host + "\r\n" +
               "Connection: close\r\n\r\n");
  unsigned long timeout = millis();
  while (client.available() == 0) {
    if (millis() - timeout > 5000) {
      Serial.println(">>> Client Timeout !");
      client.stop();
      return;
    }
  }
  // DHT11 sampling rate is 1HZ.
  delay(5000);

  
	// Do nothing here...
}
