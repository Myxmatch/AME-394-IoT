var express = require("express");
var app = express();
var bodyParser = require('body-parser');
var errorHandler = require('errorhandler');
var methodOverride = require('method-override');
var hostname = process.env.HOSTNAME || 'localhost';
var port = 1234;
var temp = 0;
var hum = 0;

app.get("/", function (req, res) {
    res.redirect("/index.html");
});

app.get("/getHum", function (req, res) {
    //res.writeHead(200, {'Content-Type': 'text/plain'});
    res.send(hum + "\r");
});
app.get("/getTemp", function (req, res) {
    //res.writeHead(200, {'Content-Type': 'text/plain'});
    res.send(temp + "\r");
});

app.get("/setValue", function (req, res) {
  var t = req.query.t;
  var h = req.query.h;
  temp = t;
  hum = h;
  res.send(temp + "," + hum);
});

app.use(methodOverride());
app.use(bodyParser());
app.use(express.static(__dirname + '/public'));
app.use(errorHandler());

console.log("Simple static server listening at http://" + hostname + ":" + port);
app.listen(port);
