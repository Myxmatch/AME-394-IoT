var nodemailer = require('nodemailer');

let transporter = nodemailer.createTransport('smtp://ame394fall2018%40gmail.com:nodemcu1234@smtp.gmail.com');
 


var express = require("express");
var app = express();
var bodyParser = require('body-parser');
var errorHandler = require('errorhandler');
var methodOverride = require('method-override');
var hostname = process.env.HOSTNAME || 'localhost';
var port = 1234;


app.get("/buttonPressed", function (req, res) {
    var id = req.query.id;
    console.log("button pressed event received");
	sendEmail();
    res.send("1");
});

function sendEmail()
{
  let message = {
    // Comma separated list of recipients
    to: 'Test account <garrett@akosweb.com>',
    subject: 'Button pressed',
    // plaintext body
    text: 'Button pressed!',
    // HTML body
    html:  '<p>Your button was presssed </p>',
    watchHtml:  '<p>Your button was presssed </p>'
  }

  console.log('Sending Mail');
  transporter.sendMail(message, function(err, result){
	console.log(err,result);
  });
   

}

app.use(methodOverride());
app.use(bodyParser());
app.use(express.static(__dirname + '/public'));
app.use(errorHandler());

console.log("Simple static server listening at http://" + hostname + ":" + port);
app.listen(port);
